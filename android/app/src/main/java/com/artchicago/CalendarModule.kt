package com.artchicago

import android.content.ContentValues
import android.content.pm.PackageManager
import android.os.Build
import android.provider.CalendarContract
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.Callback
import com.facebook.react.bridge.ReactMethod
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import android.Manifest


class CalendarModule(reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext){

    @ReactMethod
    fun createCalendarEvent(event : String, description : String ,date: String, startTime: String, endTime: String, callback: Callback) {
        if (ContextCompat.checkSelfPermission(
                reactApplicationContext,
                Manifest.permission.WRITE_CALENDAR
            ) != PackageManager.PERMISSION_GRANTED && currentActivity != null
        ) {
            ActivityCompat.requestPermissions(
                currentActivity!!,
                arrayOf(Manifest.permission.WRITE_CALENDAR),
                0
            )
        } else
        if (getMillis(date, startTime) > 0) {
             val values = ContentValues()
             values.put(CalendarContract.Events.DTSTART, getMillis(date, startTime))
             values.put(CalendarContract.Events.DTEND, getMillis(date, endTime))
             values.put(CalendarContract.Events.TITLE, event)
             values.put(CalendarContract.Events.DESCRIPTION, description)
             values.put(
                 CalendarContract.Events.CALENDAR_ID,
                 1
             )

             values.put(
                 CalendarContract.Events.EVENT_TIMEZONE,
                 "America/Bogota"
             )


             reactApplicationContext.contentResolver.insert(
                 CalendarContract.Events.CONTENT_URI,
                 values
             )

            callback.invoke(true)
         }else {
            callback.invoke(true)
        }
    }

    private fun getMillis(date : String, hour : String) : Long {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX", Locale.getDefault())
            val transFormDate = dateFormat.parse(date)
            val calendarDate = Calendar.getInstance().apply { time = transFormDate }


            val hourSplit = hour.split(":")
            calendarDate.set(Calendar.HOUR_OF_DAY, hourSplit[0].toInt())
            calendarDate.set(Calendar.MINUTE, hourSplit[1].toInt())

            return calendarDate.timeInMillis
        }
        return 0
    }

    override fun getName() = "CalendarModule"
}