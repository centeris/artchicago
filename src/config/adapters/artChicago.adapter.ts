import { AxiosAdapter } from './http/axios.adapter';


export const artChicagoFetcher = new AxiosAdapter({
  baseUrl: 'https://api.artic.edu/api/v1'
});
