import { HttpAdapter } from "../../../config/adapters/http/http.adapter.ts";
import { Base, EventResponse } from "../../../infrastructure/interfaces";
import { EventMapper } from "../../../infrastructure/mappers/event.mapper.ts";


import type { Event } from "../../../core/entities/event.entity";

export const eventUseCase = async ( fetcher: HttpAdapter, eventId : number  ):Promise<Event> => {

  try {

    const listEvents = await fetcher.get<Base<EventResponse>>(`/events/${eventId}`);

    return EventMapper.fromArtChicagoResultToEntity(listEvents.data);

  } catch (error) {
    console.log(error);
    throw new Error(`Error fetching event ${eventId}`);
  }
}
