import {HttpAdapter} from '../../../config/adapters/http/http.adapter.ts';
import {Base, EventResponse} from '../../../infrastructure/interfaces';
import {EventMapper} from '../../../infrastructure/mappers/event.mapper.ts';

import type {Event} from '../../../core/entities/event.entity';

export const eventsListUseCase = async (
  fetcher: HttpAdapter,
  page: number,
): Promise<Event[]> => {
  try {
    const listEvents = await fetcher.get<Base<EventResponse[]>>(
      `/events?page=${page}`,
    );

    return listEvents.data.map(EventMapper.fromArtChicagoResultToEntity);
  } catch (error) {
    console.log(error);
    throw new Error('Error fetching events - List Events');
  }
};
