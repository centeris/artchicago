export interface Event {
  id: number;
  title: string;
  description: string;
  shortDescription: string;
  image: string;
  startDate: Date;
  startTime: string;
  endDate: Date;
  endTime: string;
  dateDisplay: string;
}
