import React from 'react';
import {Image, Pressable, StyleSheet, View} from 'react-native';
import HTMLView from 'react-native-htmlview';
import type {Event} from '../../core/entities/event.entity';
import {NavigationProp, useNavigation} from '@react-navigation/native';
import {RootStackParams} from '../navigation/Navigation';

interface Props {
  event: Event;
}

export const Card = ({event}: Props) => {
  const navigation = useNavigation<NavigationProp<RootStackParams>>();

  return (
    <Pressable
      onPress={() => navigation.navigate('Event', {eventId: event.id})}
      style={({pressed}) => ({
        opacity: pressed ? 0.9 : 1,
        ...styles.card,
      })}>
      <View style={{flex: 0.6, minHeight: 120, width: '100%'}}>
        {
          event.image && <Image source={{uri: event.image}} style={styles.card_image} />
        }
      </View>
      <View style={styles.card_content}>
        <HTMLView value={event.shortDescription ?? event.title} />
      </View>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  card: {
    flex: 1,
    backgroundColor: '#eee',
    borderRadius: 10,
    marginHorizontal: 4,
    marginBottom: 10,
    overflow: 'hidden',

    elevation: 9,
  },
  card_image: {
    flex: 1,
    width: '100%',
    resizeMode: 'cover',
  },
  card_content: {flex: 0.4, padding: 10},
});
