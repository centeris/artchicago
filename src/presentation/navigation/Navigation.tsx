import { createStackNavigator } from '@react-navigation/stack';
import { HomeScreen } from "../screens/home/HomeScreen.tsx";
import { EventScreen } from "../screens/event/EventScreen.tsx";

export type RootStackParams = {

  Home : undefined,
  Event : { eventId  :number }

}


const Stack = createStackNavigator<RootStackParams>();

export const Navigation = () => {
  return (
    <Stack.Navigator screenOptions={{
      headerShown: false
    }}>
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen name="Event" component={EventScreen} />
    </Stack.Navigator>
  );
}
