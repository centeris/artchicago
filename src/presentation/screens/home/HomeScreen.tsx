import {useEvents} from '../../hooks/useEvents.tsx';
import MasonryList from '@react-native-seoul/masonry-list';
import {Card} from '../../components/Card.tsx';
import {FullScreenLoader} from '../../components/FullScreenLoader.tsx';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import { StyleSheet, Text, useWindowDimensions } from "react-native";

export const HomeScreen = () => {

  const {top} = useSafeAreaInsets();
  const {height, width} = useWindowDimensions();
  const {isLoading, events, loadEvents, loadNext} = useEvents();

  if (isLoading && events.length === 0) {
    return <FullScreenLoader />;
  }

  return (
    <>
      <MasonryList
        ListHeaderComponent={<Text style={{marginTop: top + 20, ...styles.listTitle}}>List Events</Text>}
        style={{marginTop: top + 20, ...styles.list}}
        data={events}
        keyExtractor={(item): string => item.id}
        numColumns={(width > height) ? 2 : 1}
        showsVerticalScrollIndicator={false}
        renderItem={({item}) => <Card event={item} />}
        refreshing={isLoading}
        onRefresh={loadEvents}
        onEndReachedThreshold={0.1}
        onEndReached={loadNext}
      />
    </>
  );
};

const styles = StyleSheet.create({
  listTitle : {
    paddingStart: 14,
    fontSize: 20,
    fontWeight: 'bold',
  },
  list : {
    paddingBottom: 30,
    paddingHorizontal: 10
  }
});
