import {useEvent} from '../../hooks/useEvent.tsx';
import {
  NativeModules,
  Image,
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  useWindowDimensions,
  View, Alert
} from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import {RootStackParams} from '../../navigation/Navigation.tsx';
import {StackScreenProps} from '@react-navigation/stack';
import {FullScreenLoader} from '../../components/FullScreenLoader.tsx';
import {useNavigation} from '@react-navigation/native';

import HTMLView from 'react-native-htmlview';
import Icon from 'react-native-vector-icons/Ionicons';
import {useEffect, useState} from 'react';
const {CalendarModule} = NativeModules;

interface Props extends StackScreenProps<RootStackParams, 'Event'> {}

export const EventScreen = ({route}: Props) => {
  const {eventId} = route.params;
  const [favorite, setFavorite] = useState(false);
  const {isLoading, event} = useEvent(eventId);
  const {height: screenHeight} = useWindowDimensions();
  const navigation = useNavigation();

  useEffect(() => {
    isFavorite();
  }, []);

  const isFavorite = async () => {
    const fav = await AsyncStorage.getItem(`favorite_${eventId}`);
    if (fav != null) {
      setFavorite(true);
    }
  };

  if (isLoading) {
    return <FullScreenLoader />;
  }

  const changeFavorite = async () => {
    if (!favorite) {
      await AsyncStorage.setItem(`favorite_${eventId}`, 'true');
    } else {
      await AsyncStorage.removeItem(`favorite_${eventId}`);
    }
    setFavorite(it => !it);
  };

  const addEvent = () => {
    CalendarModule.createCalendarEvent(event.title, event.shortDescription,event.startDate, event.startTime, event.endTime, stored => {
      Alert.alert('Save status', stored? 'The event has been successfully saved to the calendar': 'Could not create event', [
        {text: 'OK', onPress: () => {}},
      ]);
    });
  };

  return (
    <>
      <ScrollView>
        <View style={{...styles.imageContainer, height: screenHeight * 0.3}}>
          <View style={styles.imageBorder}>
            {
              event.image && <Image style={styles.image} source={{uri: event.image}} />
            }
          </View>
        </View>

        <View style={styles.marginContainer}>
          { event.dateDisplay != "No Available" && <Text style={{ fontSize: 16 }}>{event.dateDisplay}</Text>}
          <Text style={styles.title}>{event.title}</Text>
        </View>

        <View style={styles.backButton}>
          <Pressable onPress={() => navigation.goBack()}>
            <Text style={styles.backButtonText}>Back</Text>
          </Pressable>
        </View>

        <View style={styles.favoriteButton}>
          <Pressable onPress={() => changeFavorite()}>
            <Icon
              name={favorite ? 'heart' : 'heart-outline'}
              size={30}
              color={favorite ? 'red' : 'white'}
            />
          </Pressable>
        </View>

        <View style={styles.contentDescription}>
          <Text style={styles.titleDescription}>Description</Text>
          <HTMLView value={event.description} />
        </View>
        <View style={styles.buttonSection}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.buttonAdd}
            onPress={addEvent}>
            <Text style={styles.buttonAddText}>Add to Events</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  imageContainer: {
    width: '100%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.24,
    shadowRadius: 7,

    elevation: 9,
    borderBottomEndRadius: 25,
    borderBottomStartRadius: 25,
  },

  imageBorder: {
    flex: 1,
    overflow: 'hidden',
    borderBottomEndRadius: 25,
    borderBottomStartRadius: 25,
  },
  image: {
    flex: 1,
  },

  marginContainer: {
    marginHorizontal: 20,
    marginTop: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  backButton: {
    position: 'absolute',
    zIndex: 999,
    elevation: 9,
    top: 35,
    left: 10,
  },
  favoriteButton: {
    position: 'absolute',
    zIndex: 999,
    elevation: 9,
    top: 35,
    right: 10,
  },
  backButtonText: {
    color: 'white',
    fontSize: 25,
    fontWeight: 'bold',
    textShadowColor: 'rgba(0, 0, 0, 0.55)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10,
  },
  contentDescription: {
    marginHorizontal: 20,
    marginBottom: 20,
  },
  titleDescription: {fontSize: 23, marginTop: 10, fontWeight: 'bold'},
  buttonSection: {
    justifyContent: 'center',
    display: 'flex',
    alignItems: 'center',
    marginBottom: 30,
  },
  buttonAdd: {
    backgroundColor: '#4894FE',
    paddingHorizontal: 20,
    paddingVertical: 8,
    borderRadius: 18,
  },
  buttonAddText: {
    color: 'white',
    fontSize: 16,
  },
});
