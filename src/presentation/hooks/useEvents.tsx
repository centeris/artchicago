import {useEffect, useState} from 'react';
import type {Event} from '../../core/entities/event.entity';

import * as UseCases from '../../core/use-cases';
import {artChicagoFetcher} from '../../config/adapters/artChicago.adapter.ts';

export const useEvents = () => {
  const [page, setPage] = useState(1);
  const [events, setEvents] = useState<Event[]>([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    initialLoad();
  }, []);

  const initialLoad = async () => {
    setPage(1);
    loadNext();
  };

  const loadNext = async () => {
    if (!isLoading) {
      setIsLoading(true);
      const listEvents = await UseCases.eventsListUseCase(
        artChicagoFetcher,
        page,
      );

      setEvents(it => {
        if (page == 1) {
          return listEvents;
        } else {
          const finalList = listEvents.filter(
            event => it.find(item => item.id === event.id) == null,
          );
          return [...it, ...finalList];
        }
      });
      setIsLoading(false);
      setPage(it => it + 1);
    }
  };

  return {
    isLoading,
    events,
    loadEvents: initialLoad,
    loadNext,
  };
};
