import {useEffect, useState} from 'react';
import type {Event} from '../../core/entities/event.entity';

import * as UseCases from '../../core/use-cases';
import {artChicagoFetcher} from '../../config/adapters/artChicago.adapter.ts';

export const useEvent = (eventId: number) => {
  const [event, setEvent] = useState<Event>({});
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    loadEvent();
  }, []);

  const loadEvent = async () => {
    const event = await UseCases.eventUseCase(artChicagoFetcher, eventId);
    setEvent(event);
    setIsLoading(false);
  };

  return {
    isLoading,
    event,
  };
};
