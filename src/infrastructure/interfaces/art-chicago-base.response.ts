export interface Base<T> {
  pagination: Pagination | null | undefined;
  data:       T;
  info:       Info;
  config:     Config;
}

export interface Config {
  iiif_url:    string;
  website_url: string;
}

export interface Info {
  license_text:  string;
  license_links: string[];
  version:       string;
}

export interface Pagination {
  total:        number;
  limit:        number;
  offset:       number;
  total_pages:  number;
  current_page: number;
  next_url:     string;
}
