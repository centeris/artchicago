import type {EventResponse} from '../interfaces';
import type {Event} from '../../core/entities/event.entity';

export class EventMapper {
  static fromArtChicagoResultToEntity(result: EventResponse): Event {
    return {
      id: result.id,
      title: result.title_display ?? result.title,
      image: result.image_url ?? "https://st4.depositphotos.com/14953852/24787/v/1600/depositphotos_247872612-stock-illustration-no-image-available-icon-vector.jpg",
      description: result.description,
      shortDescription: result.short_description ?? '',
      startDate: result.start_date,
      startTime: result.start_time,
      endDate: result.end_date,
      endTime: result.end_time,
      dateDisplay: result.date_display ?? 'No Available',
    };
  }
}
